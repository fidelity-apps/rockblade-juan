export const environment = {
  production: true,
  api_url: "https://services.fidelityapps.mx/api/v1/branch-offices",
  api_key: "AIzaSyClbwac5I9q77i3nTOa8gELcI-m3YXZ_Xs",
  smToken: '$2y$10$LGqtRWUloKQmAt.jcUldc.PmS5LJ6l.WU2w1G4PBOTLMVKZRNQFPS',
  userToken: '$2y$10$EZlXnFrhUc4aRVwjy5/v0.3JPdFNjyeANDc64YXJ5eHD8pMsv93Q.',
  img_url: 'https://services.fidelityapps.mx/'
};
